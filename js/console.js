///////////////////////////////////////
// jQuery functions for the console
///////////////////////////////////////

/*/////

To Do:
    con()
    - test if div input actually works

/////*/

// Faster console logging, with optional description
// Also works when you don't a console
function con( inp, title, div )
{
    // Output what was input
    var output = inp;
    // If a title was provided..
    if( title ){
        // Append the title to the output
        output = '['+title+'] '+inp;
    }
    // Write output to console
    console.log( '> '+output+'<br>' );

    // If you don't have a console, pass the class/id of an empty div
    if( div ){
        // Append to fake console
        $( div ).append( '> '+output+'<br>' );
    }
}
