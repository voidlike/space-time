///////////////////////////////////////
// jQuery functions for date & time
///////////////////////////////////////

/*/////

To Do:

/////*/

// Return the current date in my typical format
function getCurDate( div, yFirst )
{
    // Use '-' to separate date if not specified
    if( !div || div == null ){ div = '-'; }
    // Grab current date and chop it up
    var c = new Date(),
        cMin = c.getMinutes(),
        cHr  = c.getHours(),
        cDay = c.getDate(),
        cMth = c.getMonth()+1,
        cYr  = c.getFullYear(),
        d;
    // If not specified as year first, go DD-MM-YYYY
    if( !yFirst || yFirst == null ){ d = cDay+''+div+''+cMth+''+div+''+cYr+' '+cHr+':'+cMin; }
    // Otherwise YYY-MM-DD
    else{ d = cYr+''+div+''+cMth+''+div+''+cDay+' '+cHr+':'+cMin; }
    // Send the date back
    return d;
}

// Find how many days are in a given month
function daysInMonth( month, year )
{
    // Months with 31 or 30 days, no Feb
    var d31 = [1,'Jan',3,'Mar',5,'May',7,'Jul',8,'Aug',10,'Oct',12,'Dec'];
    var d30 = [4,'Apr',6,'Jun',9,'Sep',11,'Nov'];
    var days;
    // Does the provided month have 31 days?
    if( $.inArray( month,d31 ) >= 0 ){ days = 31;}
    // Does the provided month have 30 days?
    else if( $.inArray( month,d30 ) >= 0 ){ days = 30;}
    else{
        // Assume a leapyear
        var days = 29;
        // Immediately override if not divisible by 4
        if( year % 4 != 0 ){ days = 28; }
        // If divisible by 4 but not 100 it's a leapyear
        else if( year % 100 != 0 ){ days = 29; }
        // If divisible by 4 but not by 400 then a common year
        else if( year % 400 != 0 ){ days = 28; }
    }
    return days;
}
