///////////////////////////////////////
// jQuery functions for the console
///////////////////////////////////////

/*/////

To Do:
    pad()
    - add decimal rounding
    - add decimal padding

/////*/

// Pad a number to a certain length
function pad( number, length )
{
    // Pad to 2 if length not specified
    if( !length || length == null ){ length = 2; }
    // Number is now a String
    number = number+'';
    var int = number, dec = '';
    // If the number is a Float, split it apart
    if( number.indexOf('.') > -1 ) {
        int = number.split('.')[0];
        dec = number.split('.')[1];
    }
    // Keep adding zeros to the int until it is the right length
    while( int.length < length ){ int = '0'+int; }
    // If there is no decimal, turn the number into a Number
    if( dec === '' ){ var full = parseInt( int,10 ); }
    // If there is a decimal, combine the int and dec into a Number
    else{ var full = parseFloat( ( int+'.'+dec ),10 ); }
    // Send it back
    return full;
}

// Create a random string of text and numbers
function randString( length, up, nums, symbs )
{
    // rand is a String
    var rand = '';
    // lowercase pool
    var pool = 'abcdefghijklmnopqrstuvwxyz';
    // if uppercase, add uppercase pool
    if( up === true ){ pool = pool+'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; }
    // if numbers, add number pool
    if( nums === true ){ pool = pool+'0123456789'; }
    // if symbols, add symbol pool
    if( symbs === true ){ pool = pool+',./?;:|`~!@#$%^&*-_=+'; }
    // randomly generate the specified amount from the pool
    for(var i=0; i < length; i++){
        rand += pool.charAt(Math.floor(Math.random() * pool.length));
    }
    // return the random string
    return rand;
}
